<?php
/**
 * Plugin Name: YouTube video post type
 */

define( 'YOUTUBE_SLUG', 'youtube' );
define( 'YOUTUBE_BRAND_NAME', 'YouTube' );
define( 'YOUTUBE_VIDEO_POST_TYPE', 'youtube-video' );
define( 'YOUTUBE_VIDEO_POST_TYPE_URL', plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) );
define( 'YOUTUBE_VIDEO_POST_TYPE_PATH', plugin_dir_path( __FILE__ ) );
define( 'YOUTUBE_USER_FIELD', 'youtube-url' );

class YouTube_Post_Type {

	public function init() {
		if ( is_admin() ) {
			load_plugin_textdomain( 'youtube-video-post-type', false, dirname( plugin_basename( __FILE__ ) ) );
			$youtube_id = get_the_author_meta( YOUTUBE_USER_FIELD, get_current_user_id() );
			if ( $youtube_id || current_user_can( 'administrator' ) ) {
				wp_register_style( 'youtube-video-admin-style', YOUTUBE_VIDEO_POST_TYPE_URL . '/style-admin.css', array(), filemtime( YOUTUBE_VIDEO_POST_TYPE_PATH . '/style-admin.css' ) );
				wp_enqueue_style( 'youtube-video-admin-style' );
				add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ) );
			} else {
				add_filter( 'admin_head', array( __CLASS__, 'hide_admin_menu_item' ) );
			}

			add_filter( 'gutenberg_can_edit_post_type', array( __CLASS__, 'gutenberg_can_edit_post_type' ), 10, 2 );
			add_filter( 'use_block_editor_for_post_type', array( __CLASS__, 'gutenberg_can_edit_post_type' ), 10, 2 );

			add_action( 'show_user_profile', array( __CLASS__, 'extra_user_profile_fields' ), 7 );
			add_action( 'edit_user_profile', array( __CLASS__, 'extra_user_profile_fields' ), 7 );
			add_action( 'personal_options_update', array( __CLASS__, 'save_extra_user_profile_fields' ) );
			add_action( 'edit_user_profile_update', array( __CLASS__, 'save_extra_user_profile_fields' ) );
			add_action( 'user_profile_update_errors', array( __CLASS__, 'validate_extra_user_profile_fields' ), 10, 3 );
		} else {
			add_filter(
				'clapclap_author_social_icons',
				function ( $social_icons, $author_id ) {
					$youtube_id = get_the_author_meta( YOUTUBE_USER_FIELD, (int) $author_id );
					if ( $youtube_id ) {
						$social_icons[] = array(
							'slug' => YOUTUBE_SLUG,
							'name' => YOUTUBE_BRAND_NAME,
							'icon' => YOUTUBE_VIDEO_POST_TYPE_URL . '/icons/brand-youtube.svg',
							'link' => 'https://www.youtube.com/channel/' . $youtube_id,
						);
					}
					return $social_icons;
				},
				5,
				2
			);
		}
		register_post_type(
			YOUTUBE_VIDEO_POST_TYPE,
			array(
				'label'              => __( 'YouTube videos', 'youtube-video-post-type' ),
				'public'             => true,
				'show_in_rest'       => true,
				'publicly_queryable' => true,
				'supports'           => array( 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'title', 'editor' ),
				'has_archive'        => true,
				'taxonomies'         => array( 'post_tag', 'category' ),
				'rewrite'            => array(
					'slug' => 'u/%author%/' . YOUTUBE_SLUG,
				),
				'capabilities'       => array(
					'create_posts' => 'do_not_allow',
				),
				'map_meta_cap'       => true,
				'menu_icon'          => YOUTUBE_VIDEO_POST_TYPE_URL . '/icons/brand-youtube.svg',
			)
		);
	}

	public static function extra_user_profile_fields( WP_User $user ) {
		if ( in_array( 'author', $user->roles, true ) ) {
			?>
		<h3><?php echo esc_html( YOUTUBE_BRAND_NAME ); ?></h3>

		<table class="form-table">
			<tr>
				<th><label for="<?php echo esc_attr( YOUTUBE_USER_FIELD ); ?>"><?php esc_html_e( 'YouTube channel ID', 'youtube-video-post-type' ); ?></label></th>
				<td>
					<input type="text" name="<?php echo esc_attr( YOUTUBE_USER_FIELD ); ?>" id="<?php echo esc_attr( YOUTUBE_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( YOUTUBE_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text"  pattern="[^()/><\][\\\x22,;|]+" />
					<div class="author-social-networks-input-explanation">
						<p><?php esc_html_e( 'Please, write a valid channel ID.', 'clapclap' ); ?></p>
					</div>
					<br><small><a href="https://support.google.com/youtube/answer/3250431" target="_blank"><?php echo esc_html__( 'Read how you can get your channel ID.', 'youtube-video-post-type' ); ?></a></small>
				</td>
			</tr>
		</table>
			<?php
		}
	}

	public static function is_valid_username( string $username ) : bool {
		return ! str_contains( $username, '/' );
	}

	public static function save_extra_user_profile_fields( int $user_id ) {
		$nonce = empty( $_POST['_wpnonce'] ) ? '' : sanitize_key( wp_unslash( $_POST['_wpnonce'] ) );
		if ( ! $nonce || ! wp_verify_nonce( $nonce, 'update-user_' . $user_id ) ) {
			return;
		}

		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}

		$user = get_user_by( 'ID', $user_id );

		if ( in_array( 'author', $user->roles, true ) && isset( $_POST[ YOUTUBE_USER_FIELD ] ) ) {
			$username = sanitize_text_field( wp_unslash( $_POST[ YOUTUBE_USER_FIELD ] ) );
			if ( self::is_valid_username( $username ) ) {
				update_user_meta( $user_id, YOUTUBE_USER_FIELD, $username );
			}
		}
	}

	public static function validate_extra_user_profile_fields( \WP_Error $errors, bool $update, stdClass $user ): void {
		$nonce = empty( $_POST['_wpnonce'] ) ? '' : sanitize_key( wp_unslash( $_POST['_wpnonce'] ) );
		if ( ! $nonce || ! wp_verify_nonce( $nonce, 'update-user_' . $user->ID ) ) {
			return;
		}

		if ( 'author' !== $user->role && isset( $_POST[ YOUTUBE_USER_FIELD ] ) ) {
			$username = sanitize_text_field( wp_unslash( $_POST[ YOUTUBE_USER_FIELD ] ) );
			if ( ! self::is_valid_username( $username ) ) {
				$errors->add( 'youtube_video_post_type', esc_html__( 'The YouTube username you introduced is not correct.', 'youtube-video-post-type' ) );
			}
		}
	}

	public static function admin_menu() {
		$menu_handle = add_submenu_page( 'edit.php?post_type=' . YOUTUBE_VIDEO_POST_TYPE, 'Import', 'Import', 'publish_posts', 'import', array( __CLASS__, 'add_import_page' ) );
		$menu_handle = add_submenu_page( null, 'Imported video', 'Imported video', 'publish_posts', 'youtube_new_video', array( __CLASS__, 'add_video_import_page' ) );
	}

	public static function hide_admin_menu_item() {
		echo '<style>#menu-posts-youtube-video{display:none}</style>';
	}

	private static function convert_duration_to_seconds( string $duration ): int {
		$start = new DateTime( '@0' );
		$start->add( new DateInterval( $duration ) );
		return $start->getTimestamp();
	}

	private static function get_author_id_from_channel_id( string $channel_id ): int {
		$args  = array(
			'role'       => 'author',
			'meta_key'   => YOUTUBE_USER_FIELD,
			'meta_value' => $channel_id,
		);
		$users = get_users( $args );

		if ( count( $users ) > 0 ) {
			return $users[0]->ID;
		}

		return 0;
	}

	private static function insert_post_from_video( string $id, $video, string $video_type = '' ) {
		$snippet         = $video->snippet;
		$status          = $video->status;
		$content_details = $video->contentDetails;

		$channel_id = $snippet->channelId;

		$author_id = self::get_author_id_from_channel_id( $channel_id );
		if ( ! $author_id ) {
			echo '<div class="notice notice-error"><p>' . esc_html__( 'You need to specify the author.', 'youtube-video-post-type' ) . '</p></div>';
			return;
		}
		if ( ! current_user_can( 'administrator' ) && get_current_user_id() !== $author_id ) {
			echo '<div class="notice notice-error"><p>' . esc_html__( 'You can only import videos from your channel.', 'youtube-video-post-type' ) . '</p></div>';
			return;
		}

		$title                  = $snippet->title;
		$description            = $snippet->description;
		$default_audio_language = $snippet->defaultAudioLanguage;
		$published_at           = $snippet->publishedAt;
		$thumbnails             = $snippet->thumbnails;
		$privacy_status         = $status->privacyStatus;
		$broadcast_content      = $snippet->liveBroadcastContent;
		$duration               = $content_details->duration;

		if ( $thumbnails->standard ) {
			$thumbnail_url = $thumbnails->standard->url;
		} elseif ( $thumbnails->high ) {
			$thumbnail_url = $thumbnails->high->url;
		}

		if ( $default_audio_language && 'ca' !== $default_audio_language ) {
			echo '<div class="notice notice-warning">' . esc_html__( 'It looks like this video is not in Catalan, are you sure you selected to correct one?', 'youtube-video-post-type' ) . '</div>';
		}

		$old_video = new WP_Query(
			array(
				'meta_key'   => 'youtube_id',
				'meta_value' => $id,
				'post_type'  => YOUTUBE_VIDEO_POST_TYPE,
			)
		);

		$post_id = 0;

		if ( count( $old_video->posts ) > 0 ) {
			$post_id = $old_video->posts[0]->ID;
		}

		$categories = array();
		if ( CLAPCLAP_VIDEOS_EXCLUSIVE_CATEGORY && 'exclusive' === $video_type ) {
			if ( 'unlisted' === $privacy_status ) {
				$categories[] = CLAPCLAP_VIDEOS_EXCLUSIVE_CATEGORY;
			} else {
				echo '<div class="notice notice-warning"><p>' . esc_html__( 'Only videos marked as Hidden in YouTube can be marked as exclusive.', 'youtube-video-post-type' ) . '</p></div>';
			}
		}
		if ( CLAPCLAP_VIDEOS_ADVANCED_CATEGORY && 'advanced' === $video_type ) {
			if ( 'unlisted' === $privacy_status ) {
				$categories[] = CLAPCLAP_VIDEOS_ADVANCED_CATEGORY;
			} else {
				echo '<div class="notice notice-warning"><p>' . esc_html__( 'Only videos marked as Hidden in YouTube can be marked as advanced.', 'youtube-video-post-type' ) . '</p></div>';
			}
		}

		if ( CLAPCLAP_VIDEOS_LIVE_CATEGORY && 'live' === $broadcast_content ) {
			$categories[] = CLAPCLAP_VIDEOS_LIVE_CATEGORY;
		}

		$new_post_id = wp_insert_post(
			array(
				'ID'            => $post_id,
				'post_author'   => $author_id,
				'post_title'    => $title,
				'post_content'  => $description,
				'post_date'     => $published_at,
				'post_status'   => 'publish',
				'post_type'     => YOUTUBE_VIDEO_POST_TYPE,
				'post_category' => $categories,
				'meta_input'    => array(
					'youtube_thumbnail' => $thumbnail_url,
					'youtube_id'        => $id,
					'duration'          => self::convert_duration_to_seconds( $duration ),
				),
			)
		);

		if ( $post_id !== $new_post_id ) {
			echo '<div class="notice notice-success"><p>' . esc_html__( 'Video imported correctly.', 'youtube-video-post-type' ) . ' <a href="' . esc_url( get_permalink( $new_post_id ) ) . '">' . esc_html__( 'Open', 'youtube-video-post-type' ) . '</a>.</p></div>';
		} else {
			echo '<div class="notice notice-success"><p>' . esc_html__( 'Video updated correctly.', 'youtube-video-post-type' ) . ' <a href="' . esc_url( get_permalink( $new_post_id ) ) . '">' . esc_html__( 'Open', 'youtube-video-post-type' ) . '</a>.</p></div>';
		}
	}

	public static function is_youtube_url( array $parsed_url ): bool {
		if ( 'https' !== $parsed_url['scheme'] && 'http' !== $parsed_url['scheme'] ) {
			return false;
		}
		if ( 'www.youtube.com' !== $parsed_url['host']
			&& 'youtube.com' !== $parsed_url['host']
			&& 'www.youtu.be' !== $parsed_url['host']
			&& 'youtu.be' !== $parsed_url['host']
		) {
			return false;
		}

		return true;
	}

	public static function parse_video_id_from_url( string $url ): string {
		$parsed_url = wp_parse_url( trim( $url ) );

		if ( ! self::is_youtube_url( $parsed_url ) ) {
			return '';
		}

		parse_str( $parsed_url['query'], $query );
		if ( '/v/' === substr( $parsed_url['path'], 0, 3 ) ) {
			return substr( $parsed_url['path'], 3 );
		}
		if ( '/watch' === $parsed_url['path'] && isset( $query['v'] ) ) {
			return $query['v'];
		}

		return '';
	}

	public static function add_import_page() {
		$accepted_conditions = get_user_meta( get_current_user_id(), 'accepted_terms_and_conditions' );
		if ( ! current_user_can( 'administrator' ) && ( ! $accepted_conditions || ! $accepted_conditions[0] ) ) {
			echo '<p><a href="./profile.php">' . esc_html__( 'You need to accept the privacy policy.', 'youtube-video-post-type' ) . '</a></p>';
			return;
		}
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Import YouTube videos', 'youtube-video-post-type' ); ?></h1>
			<form action="edit.php?post_type=<?php echo esc_attr( YOUTUBE_VIDEO_POST_TYPE ); ?>&page=youtube_new_video" method="post">
				<label for="video-id"><?php esc_html_e( 'YouTube video URL:', 'youtube-video-post-type' ); ?></label><br>
				<input id="video-id" type="url" name="video-id" placeholder="ex: https://www.youtube.com/watch?v=ExkO71B7fio" pattern="^https:\/\/(www\.)?(youtube\.com|youtu\.?be)\/watch\?v=.*" required style="min-width: 35%;" />
				<p>
					<label for="video-is-public"><input id="video-is-public" type="radio" name="video-type" value="public" checked><?php esc_html_e( 'Video is public in YouTube.', 'youtube-video-post-type' ); ?></label>
					<br>
					<label for="video-is-advanced"><input id="video-is-advanced" type="radio" name="video-type" value="advanced"><?php esc_html_e( 'Video is advanced to subscribers. That means the video will be public to all users in the future, but right now it\'s only available to subscribers.', 'youtube-video-post-type' ); ?></label>
					<br>
					<label for="video-is-exclusive"><input id="video-is-exclusive" type="radio" name="video-type" value="exclusive"><?php esc_html_e( 'Video is exclusive to subscribers. That means the video will always be exclusive to subscribers.', 'youtube-video-post-type' ); ?></label>
				</p>
			<?php wp_nonce_field(); ?>
				<div class="submit"><button class="button button-primary" name="add_via_id"><?php esc_html_e( 'Import video', 'youtube-video-post-type' ); ?></button></div>
			</form>
		</div>
		<?php
	}

	private static function print_channel_form( string $channel_url = '' ) {
		?>
		<form action="edit.php?post_type=<?php echo esc_attr( YOUTUBE_VIDEO_POST_TYPE ); ?>&page=youtube_new_channel" method="post">
		<?php
		if ( $channel_url ) {
			?>
				<input id="channel-id" type="hidden" name="channel-id" value="<?php echo esc_url_raw( $channel_url ); ?>" />
			<?php wp_nonce_field(); ?>
				<div class="submit"><button class="button button-primary" name="add_via_id"><?php esc_html_e( 'Import', 'youtube-video-post-type' ); ?></button></div>
			<?php
		} else {
			?>
				<label for="channel-id"><?php esc_html_e( 'YouTube channel URL:', 'youtube-video-post-type' ); ?></label><br>
				<input id="channel-id" type="text" name="channel-id" placeholder="ex: https://www.youtube.com/channel/UC39QbSGf29J8fWJfEQqYhyA" pattern="^https:\/\/(www\.)?(youtube\.com|youtu\.?be)\/.*" required style="min-width: 35%;" />
			<?php wp_nonce_field(); ?>
				<div class="submit"><button class="button button-primary" name="add_via_id"><?php esc_html_e( 'Import channel', 'youtube-video-post-type' ); ?></button></div>
			<?php
		}
		?>
		</form>
		<?php
	}

	public static function add_video_import_page() {
		if ( isset( $_POST['video-id'] ) && is_string( $_POST['video-id'] ) && '' !== $_POST['video-id'] ) {
			check_admin_referer();
			$id = self::parse_video_id_from_url( sanitize_text_field( wp_unslash( $_POST['video-id'] ) ) );
		}

		if ( ! is_string( $id ) || '' === $id ) {
			self::print_debug_info( esc_html__( 'Invalid video id:', 'youtube-video-post-type' ) . ' ' . $id );
			return;
		}

		self::print_debug_info( esc_html__( 'Video id:', 'youtube-video-post-type' ) . ' ' . $id );

		if ( isset( $_POST['video-type'] ) && is_string( $_POST['video-type'] ) && '' !== $_POST['video-type'] ) {
			$video_type = sanitize_key( $_POST['video-type'] );
		}

		$youtube_url           = 'https://www.googleapis.com/youtube/v3/videos?id=' . $id . '&key=' . YOUTUBE_API_KEY . '&part=id,contentDetails,snippet,status';
		$youtube_response      = wp_remote_get( $youtube_url );
		$youtube_response_body = json_decode( $youtube_response['body'] );

		if ( $youtube_response_body && $youtube_response_body->items && $youtube_response_body->items[0] ) {
			self::print_debug_info( esc_html__( 'Video data:', 'youtube-video-post-type' ), print_r( $youtube_response_body, true ) );
			self::insert_post_from_video( $id, $youtube_response_body->items[0], $video_type );
		}
	}

	public static function gutenberg_can_edit_post_type( bool $can_edit, string $post_type ) {
		return YOUTUBE_VIDEO_POST_TYPE === $post_type ? false : $can_edit;
	}

	private static function print_debug_info( string $content, string $json = '' ) {
		if ( ! current_user_can( 'administrator' ) && ( ! defined( WP_DEBUG ) || ! WP_DEBUG ) ) {
			return;
		}
		echo '<div class="notice notice-info">';
		echo '<p>' . esc_html( $content ) . '</p>';
		if ( $json ) {
			echo '<details><summary>' . esc_html_e( 'Details', 'youtube-video-post-type' ) . '</summary>';
			echo '<pre>' . esc_html( $json ) . '</pre>';
			echo '</details>';
		}
		echo '</div>';
	}
}

$youtube_post_type = new YouTube_Post_Type();
add_action( 'init', array( $youtube_post_type, 'init' ) );
